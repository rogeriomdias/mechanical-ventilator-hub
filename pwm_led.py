from machine import ADC, Pin, PWM
import time

pot = ADC(Pin(34))
pot.atten(ADC.ATTN_11DB)
pot.width(ADC.WIDTH_12BIT)

LED = PWM(Pin(26), freq=20000, duty=0)

while True:
    
    leitura_pot = pot.read()
    tensao = leitura_pot * 3.3/4095
    duty = int(tensao * 310)
    LED.duty(duty)
    time.sleep(0.2)
